------------
PREPARATIONS
------------

Before each course, the infrastructure should be created in our GCP project.

In adition, the instructor should create a public repository to provide infrastructure details (IP's)

In our GCP project there are "Instance Groups" for each kind of server, the instructor just need to set the required number of servers of each type

---

+ Required infrastructure:

- Build server

Instance Group: vsts-docker-agents
Username: vsts
Password: vsts

- Development Environment

Instance Group: microservices-lab
Username: vsts
Password: vsts

- Production Environment

Instance Group: microservices-lab
Username: vsts
Password: vsts

---

+ Environment File

 ---------------------------------------------------------------------
| # | Build Server | Development Environment | Production Environment | 
 ---------------------------------------------------------------------

---

